This is a step-by-step guide for installing some popular python deep learning frameworks on Ubuntu system (tested on my Dell laptop with Ubuntu16.04/18.04). We will be using conda for package management.

#### Install conda
A virtual environment (venv) acts as an isolated container in which you can install python packages without interfering your global setup or other virtual environments. Here we install Anaconda, an easy-to-use management tool for your venvs and python packages.

Open your terminal by keyboard shortcut Ctrl+Alt+T, and type in following command to install Anaconda.

```shell
wget https://repo.anaconda.com/archive/Anaconda2-2018.12-Linux-x86_64.sh #Download latest Anaconda distribution 
sudo apt-get install bzip2 #Install bunzip2
bash Anaconda2-2018.12-Linux-x86_64.sh -b -u #Install conda, update if possible
echo ". $HOME/anaconda2/etc/profile.d/conda.sh" >> ~/.bashrc #Adding conda to command list
source ~/.bashrc #reload bashrc for conda to kick in
```

#### Install Nvidia driver (optional, only do this if you have GPU)
If you have a GPU with sufficient VRAM (recommended 6GB or more), you can use it for a significant speed boost because GPU's have massive parallel computing power for float-point operations. This requires an Nvidia driver so that your OS can access GPU. First add Nvidia driver PPA to Ubuntu by
```shell
sudo apt-get purge nvidia-* #Uninstall existing drivers
sudo add-apt-repository ppa:graphics-drivers/ppa 
sudo apt update
```
Enter you system password to proceed and once prompted,hit Enter to accept the repository signing key. Next, install the latest driver by

__- For ubuntu16.04__
```shell
sudo apt install nvidia-410
```
__- For ubuntu18.04__
```shell
sudo apt install nvidia-driver-410
```
##### - Disable secure boot
After above installation you may see your terminal window turning to this (if not, simply reboot and skip this step) ![](img/UEFI.png)

If this happens, you probably have an OEM-manufactured PC (laptop, for example) with a feature called secure boot, which disallows installation of drivers from untrusted sources (in our case, the Nvidia driver). 

From this point on, you can either (a) disable secure boot in your UEFI settings (if you know how), or (b) proceed with the current terminal window and select OK (by hitting Tab and then Enter). 

If you choose (b), you will be asked to set up a password - remember it and reboot. Before entering Ubuntu, you will see a blue screen (MOK management), choose 'change Secure Boot state' and enter the password you typed earlier, then confirm to disable secure boot.

##### - Test dirver installation
Open your terminal and type
```shell
nvidia-smi
```
If your installation was successful, you should see something like
```shell
Thu Mar  7 10:46:48 2019       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 410.78       Driver Version: 410.78       CUDA Version: 10.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce GTX 970M    Off  | 00000000:01:00.0 Off |                  N/A |
| N/A   56C    P0    27W /  N/A |   1357MiB /  3022MiB |     40%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|    0      1365      G   /usr/lib/xorg/Xorg                           845MiB |
|    0      1915      G   compiz                                       331MiB |
|    0      2249      G   ...-token=71A24AA9D468F9A649E1240C86BB53D9    41MiB |
|    0      2438      G   ...uest-channel-token=11571423201744974942   132MiB |
+-----------------------------------------------------------------------------+

```

#### Create your deep learning venv
In the following one-liner, we create a python 2.7 venv named _myenv_  and within it, install tensorflow (keras is now integrated in tensorflow), pytorch and opencv. We also install jupyter notebook - a handy tool for python programming either locally or on a remote server.

__- if you don't have GPU/Nvidia driver, or plan not to use it:__
```shell
conda create --name myenv python=2.7 tensorflow pytorch-cpu torchvision-cpu jupyter scipy opencv
```
__- if you have Nvidia driver installed from previous step:__
```shell
conda create --name myenv python=2.7 tensorflow-gpu pytorch torchvision jupyter scipy opencv
```
Conda will create the venv and take care of package instllation; it will also install all dependencies automatically (including CUDA and CuDNN if you use GPU). When asked, type 'y' to confirm the suggested configuration and proceed to package installation.


#### Activate your venv
At this point, the venv should be good to go. Activate it by this terminal command
```shell
conda activate myenv
```
You should see `(myenv)` at the beginning of your terminal prompt, which means you are inside this venv. Now test your package installation by
```shell
python -c 'import tensorflow, torch, scipy, cv2, numpy'
``` 
If there is no error message, everything is working fine and you can run your python codes for deep learning now. If you wish to exit the venv, simply use
```shell
conda deactivate
```
Note, that you need to re-activate your venv __everytime__ you open a new terminal window (using above line) in order to run your codes in it.

#### Run a test script  for image classification (optional)
In this repo you can find an ipython file, which is a tutorial for building and training CNN on fashion MNIST dataset authoured by tensorflow/keras team. Download this repo with
```shell
git clone https://gitlab.anu.edu.au/ziangcheng/dl-install.git && cd dl-install
```

Now open jupyter notebook (be sure to activate your venv before launching jupyter) with 
```shell
jupyter notebook
```
This should open a web page in your browser, select the ipython notebook file _basic_classification.ipynb_ by clicking on it 
![](img/jupyter.png)
And if asked, select python2 kernel. 

Now read the texts and run each code cell with SHIFT+ENTER. You can also edit the codes and run the cell again to get different results.